project(key: 'ANT', name: 'AspNetDsl') {
    plan(key: 'PD', name: 'PlanDsl') {

        variables {
            variable 'pathToAppThatAlreadyBuildedDsl', 'C:/Users/Administrator/bamboo-home/xml-data/build-dir/ANT-PD-DAR/target/_PublishedWebsites/aspnet-get-started/*'
            variable 'pathToTargetFolderDsl', 'C:/Users/Administrator/bamboo-home/xml-data/build-dir/ANT-PD-DAR/target//'
            variable 'pathToZipDsl', 'C:/Users/Administrator/bamboo-home/xml-data/build-dir/ANT-PD-DAR/target/aspnet-get-started.zip'
        } 

        scm {
            git('gitRepo') {
                url 'https://bitbucket.org/ihor1306/app-service-web-dotnet-get-started.git'
                branch 'master'
                passwordAuth {
                    userName 'krivchenko1306work@gmail.com'
                    // global Bamboo variable "bamboo.gitPassword"
                    password env('gitPassword')
                }
            }
        }

        stage(name: 'Default Stage') {
            description ''
            manual false

            job(key: 'DAR', name: 'dsl after running') {
                description ''

                tasks {
                    cleanWorkingDirectory() {
                        description 'Clean the working directory'
                    }

                    checkout() {
                        description "checkout repo"
                        repository(name: 'gitRepo') {}
                    }

                    command() {
                        description 'nuget restore'
                        enabled true
						argument 'restore aspnet-get-started.sln'
                        executable 'nuget.exe'

                    }

                    script() {
                        description 'create target folder'
                        inline {
                            scriptBody '''
                                New-Item -ItemType Directory -Force -Path ${bamboo.pathToTargetFolderDsl} 
                            '''
                            interpreter ScriptInterpreter.POWERSHELL
                        }
                    }

                    msbuild(executable: 'MSBuild.exe', projectFile: 'aspnet-get-started/aspnet-get-started.csproj') {
                        description 'publish'
                        options '/p:DeployOnBuild=true /p:OutDir=${bamboo.pathToTargetFolderDsl}'
                    }

                    script() {
                        description 'create zip'
                        inline {
                            scriptBody '''
                                Compress-Archive -Path ${bamboo.pathToAppThatAlreadyBuildedDsl} -DestinationPath ${bamboo.pathToZipDsl} 
                            '''
                            interpreter ScriptInterpreter.POWERSHELL
                        }
                    }
                }

                artifacts {
                    definition(name: 'artifactDsl', copyPattern: '*.zip') {
                    location 'target'
                    shared true
                    }
                }
            }
        }
        
    
        deploymentProject(name: 'AspNetDeployDsl') {
            description 'deploy to azure using dsl'

            environment(name: 'dslDeploy') {

                description ''

                triggers {
                    afterSuccessfulBuildPlan() {
                    }
                }

                tasks {
                    cleanWorkingDirectory() {
                       description 'Clean the working directory'
                    }

                    artifactDownload() {
                        description 'Download artifact'
                        artifact(name: 'artifactDsl') {
                            sourcePlanKey 'ANT-PD'
                        }   
                    }
        
                    script() {
                        description 'deploy application to azure'
                        inline {
                            description 'deploy application'
                            enabled true
                            argument '-WebAppName "krivchenko" -ResourceGroup "test" -PathToOutputFile "C:/bamboo/aspnet-get-started/scripts/credentials" -Format "Ftp" -PathToArtifact "C:/Users/Administrator/bamboo-home/xml-data/build-dir/ANT-PD-DAR/target/aspnet-get-started.zip"'
                            interpreter ScriptInterpreter.POWERSHELL
                            scriptBody '''
                                param(
                                [Parameter(Mandatory = $true)]
                                [ValidateNotNullOrEmpty()]
                                [string]
                                $WebAppName,

                                [Parameter(Mandatory = $true)]
                                [ValidateNotNullOrEmpty()]
                                [string]
                                $ResourceGroup,

                                [Parameter(Mandatory = $true)]
                                [ValidateNotNullOrEmpty()]
                                [string]
                                $PathToOutputFile,

                                [Parameter(Mandatory = $true)]
                                [ValidateSet("Ftp")]
                                [string]
                                $Format,

                                [Parameter(Mandatory = $true)]
                                [ValidateNotNullOrEmpty()]
                                [string]
                                $PathToArtifact
                            )

                            #Parameters from bamboo variables
                            $password = ConvertTo-SecureString -String ${bamboo.PasswordSecret} -AsPlainText -Force
                            $credential = New-Object -TypeName "System.Management.Automation.PSCredential" -ArgumentList ${bamboo.UserSecret}, $password

                            Connect-AzAccount -Credential $Credential

                            $publishProfile = [xml](Get-AzWebAppPublishingProfile -ResourceGroupName $ResourceGroup -Name $webAppName -Format $format -OutputFile $PathToOutputFile)
                            $Username = (Select-Xml -Xml $publishProfile -XPath "//publishData/publishProfile[contains(@profileName,'Web Deploy')]/@userName").Node.Value
                            $Password = (Select-Xml -Xml $publishProfile -XPath "//publishData/publishProfile[contains(@profileName,'Web Deploy')]/@userPWD").Node.Value
                            $SiteName = (Select-Xml -Xml $publishProfile -XPath "//publishData/publishProfile[contains(@profileName,'Web Deploy')]/@msdeploySite").Node.Value
                            $publishUrl = (Select-Xml -Xml $publishProfile -XPath "//publishData/publishProfile[contains(@profileName,'Web Deploy')]/@publishUrl").Node.Value

                            $msdeploy = "C:/Program Files/IIS/Microsoft Web Deploy V3/msdeploy.exe"
                            & $msDeploy -verb:sync `
                                -source:package=$PathToArtifact `
                                -dest:auto,computerName="https://${publishUrl}/msdeploy.axd?site=${SiteName}",userName="${Username}",password="${Password}",authtype="Basic",includeAcls="False" `
                                -setParamFile:C:/deployment/example.com.SetParameters.xml
                            '''
                        }
                    }
                }
            }
        }
    }

    plan(key: 'PD2', name: 'PlanDsl2') {

        scm {
            git('gitRepoArm') {
                url 'https://bitbucket.org/ihor1306/repository-for-arm.git'
                branch 'master'
                passwordAuth {
                    userName 'krivchenko1306work@gmail.com'
                    // global Bamboo variable "bamboo.gitPassword"
                    password env('gitPassword')
                }
            }
        }
        
        stage(name: 'Default Stage2') {
            description ''
            enabled true

            job(key: 'DAR2', name: 'dsl after running2') {
                description ''
                
                tasks {
                    cleanWorkingDirectory() {
                        description 'Clean the working directory'
                    }

                    checkout() {
                        description "checkout repo"
                        repository(name: 'gitRepoArm') {}
                    }
                    script() {
                        description 'deploy application to azure'
                        inline {
                            description 'arm web app and slots'
                            enabled true
                            interpreter ScriptInterpreter.POWERSHELL
                            scriptBody '''
                                ./deploy.ps1 -ResourceGroupName "test" -Location "eastUs" -TemplateFile "deploy_site.json" -TemplateParameterFile "deploy_site_parameters.json"
                            '''
                        }
                    }
                }
            }
        }
    }
}